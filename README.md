# Migrating data using Rsync

To migrate the files from one PC to another you need to specify:

```
# Define the source and destination directories
source_dir = '/opt/data'
destination_base_dir = '/opt/data'
min_size_in_source = 1 * 1024 * 1024 * 1024  # 1 GB in bytes
target_pc_ip = '192.168.169.86'
ssh_user = 'bluesky'
```
min_size_in_source defined the size of the files that should stay on the source PC.

Before migrating the files be sure to copy ssh key to the target PC.

The script will sort all the files according to the creation date and then migrate those that exceed the min_size_in_source.