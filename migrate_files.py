import os
import subprocess

# Define the source and destination directories
source_dir = '/opt/data'
destination_base_dir = '/opt/data'
min_size_in_source = 1 * 1024 * 1024 * 1024  # 1 GB in bytes
target_pc_ip = '192.168.169.86'
ssh_user = 'bluesky'

# Function to get the total size of files in a directory
def get_dir_size(directory):
    total_size = 0
    for dirpath, dirnames, filenames in os.walk(directory):
        for filename in filenames:
            filepath = os.path.join(dirpath, filename)
            total_size += os.path.getsize(filepath)
    return total_size

# Function to transfer files with rsync and checksum verification
def transfer_files(source, destination_base, min_size, target_ip, ssh_user):
    # Get list of files in the source directory
    files = []
    for dirpath, dirnames, filenames in os.walk(source):
        for filename in filenames:
            filepath = os.path.join(dirpath, filename)
            files.append(filepath)

    # Sort files by modification time (oldest first)
    files.sort(key=lambda x: os.path.getmtime(x))

    # Transfer files while maintaining the minimum size in the source directory
    total_size_in_source = get_dir_size(source)
    print(f"Initial total size in source: {total_size_in_source} bytes")
    for file in files:
        if total_size_in_source <= min_size:
            print("Minimum size reached, stopping transfer")
            break

        file_size = os.path.getsize(file)
        relative_dir = os.path.dirname(os.path.relpath(file, source))
        destination_dir = os.path.join(destination_base, relative_dir)
        destination_path = os.path.join(destination_dir, os.path.basename(file))

        # Debug print statements
        print(f"Transferring file: {file}")
        print(f"File size: {file_size} bytes")
        print(f"Relative dir: {relative_dir}")
        print(f"Destination dir: {destination_dir}")
        print(f"Destination path: {destination_path}")

        # Ensure the destination directory exists
        ssh_command = f"ssh {ssh_user}@{target_ip} 'mkdir -p {destination_dir}'"
        subprocess.run(ssh_command, shell=True, check=True)

        # Use rsync to transfer the file with checksum verification
        rsync_command = f"rsync -az --remove-source-files --checksum {file} {ssh_user}@{target_ip}:{destination_path}"
        print(f"Running command: {rsync_command}")
        result = subprocess.run(rsync_command, shell=True)

        # Check if rsync command was successful
        if result.returncode == 0:
            total_size_in_source -= file_size
            print(f"Successfully transferred and removed file: {file}")
            print(f"New total size in source: {total_size_in_source} bytes")
        else:
            print(f"Failed to transfer file: {file}")

# Perform the transfer
transfer_files(source_dir, destination_base_dir, min_size_in_source, target_pc_ip, ssh_user)
